package co.simplon.promo18.firstrest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

  @GetMapping ("/api/example")
  public String firstRoute(){
    return "quelque chose";
  }
  @GetMapping ("/api/withparam/{name}")
  public String paramExample(@PathVariable String name){
    return "Coucou"+name;

  }
}
