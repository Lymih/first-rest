package co.simplon.promo18.firstrest.controller;

import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.firstrest.entity.Operation;
import co.simplon.promo18.firstrest.repository.OperationRepository;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/operation")
@Validated
public class OperationController {

  @Autowired
  OperationRepository opRepo;

  @GetMapping
  public List<Operation> all() {
    return opRepo.findAll();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)

  public Operation post(@Valid @RequestBody Operation operation) {
    if (operation.getDate() == null) {
      operation.setDate(LocalDate.now());
    }
    opRepo.save(operation);
    return operation;
  }

  @PostMapping("/add")
  @ResponseStatus(HttpStatus.CREATED)
  public Operation add(@RequestParam @NotBlank String label,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PastOrPresent LocalDate date,
      @RequestParam double amount) {
    Operation operation = new Operation(label, date, amount);
    post(operation);
    return operation;
  }

  private Operation showOne(int id) {
    Operation operation = opRepo.findById(id);
    if (operation == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return operation;
  }

  @GetMapping("/{id}")
  public Operation getOne(@PathVariable @Min(1) int id) {
    Operation operation = showOne(id);
    return operation;
  }

  @GetMapping("/show")
  @ResponseBody
  public Operation getOneByParam(@RequestParam @Min(1) int id) {
    Operation operation = showOne(id);

    return operation;

  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!opRepo.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);

    }
  }

  @PutMapping("/{id}")
  public Operation update(@RequestBody Operation operation, @PathVariable int id) {

    if (operation.getId() != id) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!opRepo.update(operation)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return operation;
  }

  @PatchMapping("/{id}")
  public void patch(@RequestBody Operation operation, @PathVariable int id) {
    Operation opToChange = opRepo.findById(id);
    if (operation.getLabel() != null) {
      opToChange.setLabel(operation.getLabel());
    }
    if (operation.getDate() != null) {
      opToChange.setDate(operation.getDate());
    }
    if (operation.getAmount() != 0.0) {
      opToChange.setAmount(operation.getAmount());
    }
    if (!opRepo.update(opToChange)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

  }


}
