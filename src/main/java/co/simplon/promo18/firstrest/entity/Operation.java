package co.simplon.promo18.firstrest.entity;

import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;


public class Operation {
  
  private Integer id;

  @NotBlank
  private String label;

  @PastOrPresent
  private LocalDate date;
  
  private double amount;

  public Operation() {}
  
  public Operation(String label, LocalDate date, double amount) {
    this.label = label;
    this.date = date;
    this.amount = amount;
  }
  public Operation(int id, String label, LocalDate date, double amount) {
    this.id = id;
    this.label = label;
    this.date = date;
    this.amount = amount;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

}
