package co.simplon.promo18.firstrest.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.firstrest.entity.Operation;

@Repository
public class OperationRepository {

  @Autowired
  private DataSource dataSource;

  public List<Operation> findAll() {
    List<Operation> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM operation");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Operation operation = new Operation(rs.getInt("id"), rs.getString("label"),
            rs.getDate("date").toLocalDate(), rs.getDouble("amount"));
        list.add(operation);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return list;
  }

  public Operation findById(int id) {

    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM operation WHERE id=?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Operation operation = new Operation(rs.getInt("id"), rs.getString("label"),
            rs.getDate("date").toLocalDate(), rs.getDouble("amount"));
        return operation;
      }


    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
    return null;
  }

  public void save(Operation operation) {
    try (Connection connection = dataSource.getConnection()) {

      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO operation (label,date,amount) VALUES (?,?,?)",
              PreparedStatement.RETURN_GENERATED_KEYS);

      stmt.setString(1, operation.getLabel());
      stmt.setDate(2, Date.valueOf(operation.getDate()));
      stmt.setDouble(3, operation.getAmount());
      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();

      if (rs.next()) {
        operation.setId(rs.getInt(1));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }


  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM operation WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");

    }

  }
  public boolean update(Operation operation){
   try ( Connection connection = dataSource.getConnection()) {
   PreparedStatement stmt = connection.prepareStatement("UPDATE operation SET label=?, date=?,amount=? WHERE id=?");
    stmt.setString(1,operation.getLabel());
    stmt.setDate(2, Date.valueOf(operation.getDate()));
    stmt.setDouble(3,operation.getAmount());

    stmt.setInt(4,operation.getId());
   return (stmt.executeUpdate() == 1);
  } catch (SQLException e) {
    e.printStackTrace();
    throw new RuntimeException("Database access error");
  }
  }
}
