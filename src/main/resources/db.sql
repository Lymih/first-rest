DROP TABLE IF EXISTS operation;
CREATE TABLE operation (
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(164),
  date DATE NOT NULL,
  amount DOUBLE NOT NULL
  )DEFAULT CHARSET UTF8;

  INSERT INTO operation (label,date,amount) VALUES ('cloth','2021-10-20',32.50),("groceries", "2022-04-28", -26.32),("gift", "2022-01-14", 50);


