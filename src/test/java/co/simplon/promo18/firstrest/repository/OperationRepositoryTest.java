package co.simplon.promo18.firstrest.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import co.simplon.promo18.firstrest.entity.Operation;

@SpringBootTest
@Sql("/db.sql")
public class OperationRepositoryTest {

  @Autowired
  OperationRepository repo;

  @Test
  void testFindAll() {

    List<Operation> result = repo.findAll();
    assertEquals(3, result.size());
  }

  @Test
  void testFindById() {
    Operation operation = repo.findById(3);
    assertEquals(50, operation.getAmount());

  }

  @Test
  void testSave() {
    Operation operation = new Operation("Operation test", LocalDate.of(2022, 05, 01), 42.00);
    repo.save(operation);
  


  }
}
